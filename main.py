from configparser import ConfigParser
import paramiko
import os
import sys
from time import sleep
from sys import platform as _platform

def getInfo():
	info.append('Hostname: ' + hostname + '\n')
	info.append('Username: ' + username + '\n')

	if uptime:
            stdin, stdout, stderr = ssh.exec_command("uptime | tail -n 1 | awk '{print $1}'")
            stdin.close()
            result = stdout.readlines()
            info.append('Uptime: ' + result[0])

	if kernel:
            stdin, stdout, stderr = ssh.exec_command("uname -r")
            stdin.close()
            result = stdout.readlines()
            info.append('Kernel: ' + result[0])

	if memory:
            stdin, stdout, stderr = ssh.exec_command("egrep --color 'MemTotal' /proc/meminfo | egrep '[0-9.]{4,}' -o")
            stdin.close()
            resultmemtotal = stdout.readlines() # Это вся память
            stdin, stdout, stderr = ssh.exec_command("egrep --color 'MemFree' /proc/meminfo | egrep '[0-9.]{4,}' -o")
            stdin.close()
            memFree = stdout.readlines() # Это вся свободная память
            memUsed = int(resultmemtotal[0]) - int(memFree[0]) # Высчитываем сколько памяти использовано
            persentUsed = round(memUsed*100/int(resultmemtotal[0])) # Высчитываем сколько процентов памяти использовано
            persentFree = 100 - persentUsed # Высчитываем процент свободной памяти (не выводится)
            info.append('Memory: ' + str(persentUsed) + '%/100%' + '\n')

	if temp:
            stdin, stdout, stderr = ssh.exec_command("cat /sys/class/thermal/thermal_zone0/temp")
            stdin.close()
            result = stdout.readlines()
            try:
                resulttemp = round(int(result[0])/1000) # Приводим вывод в нормальный вид
            except IndexError:
                resulttemp = 'unknown'
            info.append('Temperature: ' + str(resulttemp) + '°C')

	if top:
            stdin, stdout, stderr = ssh.exec_command("ps aux --width 30 --sort -rss --no-headers | head  | awk '{print $11}'")
            stdin.close()
            result = stdout.readlines()
            processes = []
            for i in range(len(result)):
                processes.append(result[i].strip('\n') + ';')
            info.append('\nTOP: ' + '\n' + '\n'.join(processes))

def appendError(err):
    """
    Эта функция отвечает за логгирование ошибок
    Пока не использована
    """
    with open("error.log", "a") as logfile:
        logfile.write(err)

def clear():
    """
    Эта функция отвечает за подбор команды для очищения экрана, в зависимости от платформы
    """
    if platform == 'linux' or platform == 'darwin':
        os.system('clear')
    elif platform == 'windows':
        os.system('cls')


def generateConfig():
    """
    Эта функция отвечает за генерацию стандартного конфига, если его нет
    """
    f = open("settings.ini", "w")

    parser.add_section("default")
    parser.set("default", "hostname", "localhost")
    parser.set("default", "port", "22")
    parser.set("default", "username", "pi")
    parser.set("default", "password", "")

    parser.add_section("settings")
    parser.set("settings", "interval", "10")
    parser.set("settings", "uptime", "1")
    parser.set("settings", "kernel", "1")
    parser.set("settings", "memory", "1")
    parser.set("settings", "temp", "1")
    parser.set("settings", "top", "1")
    parser.write(f)
    f.close()

def main():
    """
    Главная функция
    Отвечает за подключение к клиенту по SSH, изменение размера терминала и вывод информации
    """
    clear()
    global ssh
    ssh = paramiko.SSHClient()  # Определяем наш ssh клиент
    ssh.set_missing_host_key_policy(paramiko.client.AutoAddPolicy()) # Изменяем политику, в противном случае мы не подключимся
    try:
        ssh.connect(hostname, int(port), username, password) # Подключаемся по ssh к серверу
    except paramiko.ssh_exception.AuthenticationException: # Ловим ошибку авторизации и предотвращаем краш
        print('Не удалось подключиться, возможно неправильный пароль.\n')
        return 228
    if resize: # Если rezise включен, то терминал будет менять размер
        if platform == 'windows' or _platform == 'cygwin': # Если Windows или Cygwin, то меняем по особенному
            if not top:
                os.system('mode con cols=16 lines=8') # Если TOP выключен, то делаем окно размером 16x8
            else:
                os.system('mode con cols=30 lines=30')
        if platform == 'linux' or _platform == 'darwin': # Если Linux, то меняем по особенному
            #if os.environ['TERM'] == 'xterm': # Меняем только если терминал - xterm, с другими не работает
                if top:
                    os.system("printf '\e[8;30;30t'")
                else:
                    os.system("printf '\e[8;24;80t'")
    global info
    while True: # Переводим программу в вечный цикл
        info = []
        clear()
        getInfo()
        print('\n'.join(info))
        sleep(float(interval))

# Определяем платформу
if _platform == "linux" or _platform == "linux2":
    platform = 'linux'
elif _platform == "darwin":
    platform = 'darwin'
elif _platform == "win32":
    platform = 'windows'

parser = ConfigParser() # Определяем парсер конфига

if (os.path.isfile("settings.ini")): # Если settings.ini существует
    parser.read('settings.ini') # Парсим конфиг
else:
    print("Не найден конфиг, генерируем стандартный settings.ini")
    generateConfig() # Если конфига нет, то генерируем стандартный
    print("Конфиг успешно сгенерирован. Отредактируйте его под себя!")
    sleep(10)
    sys.exit()

try:
    hostname = parser.get('default', 'hostname') # Парсим хост
    port = parser.get('default', 'port')         # Парсим порт ssh сервера
    username = parser.get('default', 'username') # Парсим имя пользователя
    password = parser.get('default', 'password') # Парсим пароль
    interval = parser.get('settings', 'interval') # Парсим интервал обновления
    # Парсим другие настройки
    uptime = bool(parser.get('settings', 'uptime'))
    kernel = bool(parser.get('settings', 'kernel'))
    memory = bool(parser.get('settings', 'memory'))
    temp = bool(parser.get('settings', 'temp'))
    top = bool(parser.get('settings', 'top'))
    resize = bool(parser.get('settings', 'top'))
except Exception:                                # При любой ошибке, связанной с конфигом, предотвращаем краш
    print('Отредактируйте конфиг, пожалуйста.')
    sleep(10)

main()
